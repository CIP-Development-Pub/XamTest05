﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamTest05
{
    public partial class MainPage : ContentPage
    {
        ObservableCollection<Product> productList;

        public MainPage()
        {
            InitializeComponent();

            productList = new ObservableCollection<Product>();
            ListViewBarcodes.ItemsSource = productList;
        }
        
        private void ButtonAdd_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(EntryBarcode.Text))
            {
                EntryBarcode.Focus();
                return;
            }

            productList.Add(new Product(EntryBarcode.Text));
            EntryBarcode.Text = "";
        }
    }

    class Product
    {   
        public Product(string barcode)
        {
            Barcode = barcode;
        }

        public string Barcode { set; get; }
        public string Name { set; get; }
    }
}
